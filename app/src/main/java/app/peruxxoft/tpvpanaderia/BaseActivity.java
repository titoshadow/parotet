package app.peruxxoft.tpvpanaderia;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.SQLException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

import data.Articulo;
import data.ParotetDB;
import io.github.yavski.fabspeeddial.FabSpeedDial;
import io.github.yavski.fabspeeddial.SimpleMenuListenerAdapter;

public class BaseActivity extends AppCompatActivity {

    static final int PICK_PHOTO_ARTICLE = 666;
    static final String OPTION_MENU = "Opciones";
    static final String ARTICLE_MENU = "Artículo";
    static final String CATEGORY_MENU = "Categoría";
    static final int READ_PERMISSION = 100;
    static final int WRITE_PERMISSION = 101;
    static final String PHOTO_BYTES = "idFoto";
    final ParotetDB db = new ParotetDB(this);
    ImageView fotoArticulo = null;
    boolean pressedNum;
    boolean pressedImage;
    boolean pressedItemList;
    boolean dobleAtras = false;
    // Creo que esto me lo puedo ahorrar si quito el textCalc.getText().toString() de los botones numéricos ...
    boolean resetScreen = false;
    int posicion;
    ListAdapter adapter = null;
    byte[] png;
    private TextView textCalc;
    private String screen;
    private String nombreArticulo;
    private double precioArticulo;
    private String fotoPath;
    private TableLayout tablaPanaderia, tablaBolleria;
    private Map<String, Double> listaPrecios = new HashMap<>();
    private Map<String, Integer> pedido = new HashMap<>();
    private ArrayList<String> listaArticulos = new ArrayList<>();
    private int elemInRow = 0;
    private int actualRow;
    private Uri fileUri;
    private Drawable foto;
    private Properties p;
    private PropertyReader propertyReader;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_base);

        final Context context = getApplicationContext();
        // Leemos configuración inicial guardada
        propertyReader = new PropertyReader(context);
        p = propertyReader.getProperties("MyStringsFile.properties");

        // Cogemos las tablas para meter aquí los botones
        tablaPanaderia = (TableLayout) findViewById(R.id.tablaPanaderia);
        tablaBolleria = (TableLayout) findViewById(R.id.tablaBolleria);

        final TabHost tabHost = (TabHost) findViewById(R.id.tabHost);
        tabHost.setup();

        // Añadir las categorías en tabs

        TabHost.TabSpec tab1 = tabHost.newTabSpec("Panaderia");
        TabHost.TabSpec tab2 = tabHost.newTabSpec("Bolleria");

        tab1.setIndicator("Panadería");
        tab1.setContent(R.id.panaderiaTab);

        tab2.setIndicator("Bollería");
        tab2.setContent(R.id.bolleriaTab);

        tabHost.addTab(tab1);
        tabHost.addTab(tab2);

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listaArticulos);

        // Inicializar listaPrecios a partir de la BD desde Articulos
        Cursor cArticulos = db.leerArticulos();

        // Inicializar ImageButtons -y categorías- a partir de BD (desde Artículos)
        if (cArticulos.moveToFirst()) {
            Resources res = context.getResources();
            Drawable d;
            while (cArticulos.moveToNext()) {
                File file = new File(cArticulos.getString(4));
                if (file.exists()) {
                    d = Drawable.createFromPath(cArticulos.getString(4));
                } else {
                    System.out.println("No he encontrado los archivos... en BD dice ->" + cArticulos.getString(4));
                    d = res.getDrawable(R.drawable.unknown, this.getTheme());
                }

                System.out.println(cArticulos.getColumnIndex("nombre") + " " + cArticulos.getColumnCount() + " "
                        + cArticulos.getString(1) + "\t" + cArticulos.getString(4));
                ImageButton dummy = new ImageButton(this);
                // Aquí habrá que cambiar el .getCurrentTabView por algo para coger las categorías y tal
                int idTabSelected = res.getIdentifier("tabla" + tabHost.getCurrentTabTag(), "id", context.getPackageName());
                TableLayout tabAux = (TableLayout) findViewById(idTabSelected);
                addArticuloUI(dummy, tabAux, cArticulos.getString(1), d);
                listaPrecios.put(cArticulos.getString(1), cArticulos.getDouble(3));
            }
        } else {
            System.out.println("Igual está vacía la BD ... al menos, de Artículos");
        }
        cArticulos.close();

        System.out.println(listaPrecios.toString());

        textCalc = (TextView) findViewById(R.id.textCalc);
        // Strings para @strings
        final Button botonC0 = (Button) findViewById(R.id.buttonC0);
        botonC0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (resetScreen) textCalc.setText(null);
                screen = textCalc.getText().toString() + "0";
                textCalc.setText(screen);
                pressedNum = true;
                resetScreen = false;
            }
        });

        final Button botonC1 = (Button) findViewById(R.id.buttonC1);
        botonC1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (resetScreen) textCalc.setText(null);
                screen = textCalc.getText().toString() + "1";
                textCalc.setText(screen);
                pressedNum = true;
                resetScreen = false;
            }
        });

        final Button botonC2 = (Button) findViewById(R.id.buttonC2);
        botonC2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (resetScreen) textCalc.setText(null);
                screen = textCalc.getText().toString() + "2";
                textCalc.setText(screen);
                pressedNum = true;
                resetScreen = false;
            }
        });

        final Button botonC3 = (Button) findViewById(R.id.buttonC3);
        botonC3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (resetScreen) textCalc.setText(null);
                screen = textCalc.getText().toString() + "3";
                textCalc.setText(screen);
                pressedNum = true;
                resetScreen = false;
            }
        });

        final Button botonC4 = (Button) findViewById(R.id.buttonC4);
        botonC4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (resetScreen) textCalc.setText(null);
                screen = textCalc.getText().toString() + "4";
                textCalc.setText(screen);
                pressedNum = true;
                resetScreen = false;
            }
        });

        final Button botonC5 = (Button) findViewById(R.id.buttonC5);
        botonC5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (resetScreen) textCalc.setText(null);
                screen = textCalc.getText().toString() + "5";
                textCalc.setText(screen);
                pressedNum = true;
                resetScreen = false;
            }
        });

        final Button botonC6 = (Button) findViewById(R.id.buttonC6);
        botonC6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (resetScreen) textCalc.setText(null);
                screen = textCalc.getText().toString() + "6";
                textCalc.setText(screen);
                pressedNum = true;
                resetScreen = false;
            }
        });

        final Button botonC7 = (Button) findViewById(R.id.buttonC7);
        botonC7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (resetScreen) textCalc.setText(null);
                screen = textCalc.getText().toString() + "7";
                textCalc.setText(screen);
                pressedNum = true;
                resetScreen = false;
            }
        });

        final Button botonC8 = (Button) findViewById(R.id.buttonC8);
        botonC8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (resetScreen) textCalc.setText(null);
                screen = textCalc.getText().toString() + "8";
                textCalc.setText(screen);
                pressedNum = true;
                resetScreen = false;
            }
        });

        final Button botonC9 = (Button) findViewById(R.id.buttonC9);
        botonC9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (resetScreen) textCalc.setText(null);
                screen = textCalc.getText().toString() + "9";
                textCalc.setText(screen);
                pressedNum = true;
                resetScreen = false;
            }
        });

        Button botonCAC = (Button) findViewById(R.id.buttonCAC);
        botonCAC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textCalc.setText(null);
                listaArticulos.clear();
                pedido.clear();
                ((ArrayAdapter) adapter).notifyDataSetChanged();
                pressedImage = false;
                pressedNum = false;
                resetScreen = true;
            }
        });

        final Button botonCX = (Button) findViewById(R.id.buttonCX);
        botonCX.setEnabled(false);
        botonCX.setAlpha(0.5f);
        botonCX.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pressedItemList) {
                    // Strings para @strings
                    final String aborrar = listaArticulos.get(posicion);
                    listaArticulos.remove(listaArticulos.get(posicion));
                    ((ArrayAdapter) adapter).notifyDataSetChanged();
                    System.out.println(aborrar + " ---- " + aborrar.replaceAll("x[0-9]", "").trim());
                    final int aux = pedido.get(aborrar.replaceAll("x[0-9]", "").trim());
                    pedido.remove(aborrar);
                    CharSequence chars = "Se ha borrado " + aborrar;
                    final FabSpeedDial auxFAB = (FabSpeedDial) findViewById(R.id.addImgButton);
                    Snackbar.make(auxFAB, chars, Snackbar.LENGTH_LONG)
                            .setAction("DESHACER", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    listaArticulos.add(aborrar);
                                    pedido.put(aborrar.replaceAll("x[0-9]", "").trim(), aux);
                                    ((ArrayAdapter) adapter).notifyDataSetChanged();
                                    Snackbar.make(auxFAB, "Recuperado " + aborrar, Snackbar.LENGTH_LONG).show();
                                }
                            }).show();
                    botonCX.setEnabled(false);
                    botonCX.setAlpha(0.5f);
                    pressedItemList = false;
                } else {
                    Toast.makeText(context, "No has elegido nada que borrar", Toast.LENGTH_SHORT).show();
                }
            }
        });

        ImageButton backSpaceButton = (ImageButton) findViewById(R.id.backSpaceButton);
        backSpaceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                screen = textCalc.getText().toString();
                if (!screen.isEmpty())
                    textCalc.setText(screen.substring(0, screen.length() - 1));
                resetScreen = false;
            }
        });

        Button botonT = (Button) findViewById(R.id.buttonT);
        botonT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetScreen = true;
                // Garrapiña
                textCalc.setText(((Math.round((getTotal()) * 100.0) / 100.0) + ""));
            }
        });

        final ListView listaPedidos = (ListView) findViewById(R.id.listPedido);
        listaPedidos.setAdapter(adapter);
        listaPedidos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                botonCX.setEnabled(true);
                botonCX.setAlpha(1f);
                pressedItemList = true;
                System.out.println(position + " es la posición clicada");
                posicion = position;
            }
        });


        final FabSpeedDial addButton = (FabSpeedDial) findViewById(R.id.addImgButton);
        addButton.setLongClickable(true);

        addButton.setMenuListener(new SimpleMenuListenerAdapter() {
            @Override
            public boolean onMenuItemSelected(MenuItem menuItem) {
                String menuItemPressed = menuItem.getTitle().toString();
                switch (menuItemPressed) {
                    case OPTION_MENU:
                        Intent intent = new Intent(context, MenuActivity.class);
                        startActivity(intent);
                        break;
                    case CATEGORY_MENU:
                        break;
                    case ARTICLE_MENU:
                        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(BaseActivity.this);
                        View mView = layoutInflaterAndroid.inflate(R.layout.dialog_article, null);
                        AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(new ContextThemeWrapper(BaseActivity.this, R.style.AppTheme));
                        alertDialogBuilderUserInput.setView(mView);
                        alertDialogBuilderUserInput.setIcon(R.drawable.ic_backspace_white_24dp);
                        fotoArticulo = (ImageView) mView.findViewById(R.id.fotoArticulo);
                        fotoArticulo.setClickable(true);
                        fotoArticulo.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                // Simplificado

                                //Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
                                //getIntent.setType("image/*");
                                if (ContextCompat.checkSelfPermission(BaseActivity.this,
                                        Manifest.permission.READ_EXTERNAL_STORAGE)
                                        != PackageManager.PERMISSION_GRANTED) {

                                    ActivityCompat.requestPermissions(BaseActivity.this,
                                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, WRITE_PERMISSION);
                                } else {
                                    Intent pickIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                    pickIntent.setType("image/*");
                                    pickIntent.putExtra(PHOTO_BYTES, png);

                                    startActivityForResult(pickIntent, PICK_PHOTO_ARTICLE);
                                }
                            }
                        });

                        final EditText nombreNuevoArticulo = (EditText) mView.findViewById(R.id.nombre);
                        final EditText precioNuevoArticulo = (EditText) mView.findViewById(R.id.precio);
                        precioNuevoArticulo.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                        final ImageView fotoNuevoArticulo = (ImageView) mView.findViewById(R.id.fotoArticulo);

                        alertDialogBuilderUserInput
                                .setCancelable(false)
                                .setPositiveButton("Añadir", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogBox, int id) {
                                        // Garrapiña
                                        if (!nombreNuevoArticulo.getText().toString().isEmpty() && !precioNuevoArticulo.getText().toString().isEmpty()) {
                                            nombreArticulo = nombreNuevoArticulo.getText().toString();
                                            try {
                                                Resources res = getResources();
                                                precioArticulo = Double.parseDouble(precioNuevoArticulo.getText().toString().replace(",", "."));
                                                // Guardamos artículo en BD
                                                // Strings para @strings
                                                if (db.crearArticulo(new Articulo(nombreArticulo, precioArticulo, tabHost.getCurrentTabTag(), fotoPath))) {
                                                    ImageButton dummy = new ImageButton(BaseActivity.this);
                                                    listaPrecios.put(nombreArticulo, precioArticulo);
                                                    // Garrapiña maxima

                                                    int idTabSelected = res.getIdentifier("tabla" + tabHost.getCurrentTabTag(), "id", context.getPackageName());
                                                    TableLayout tabAux = (TableLayout) findViewById(idTabSelected);

                                                    addArticuloUI(dummy, tabAux, nombreArticulo, foto);
                                                    foto = null; // Dejamos vacío el puntero al drawable del artículo
                                                } else {
                                                    Toast.makeText(context, "No se ha podido crear el artículo en BD", Toast.LENGTH_LONG).show();
                                                }
                                            } catch (NumberFormatException e) {
                                                Toast.makeText(context, "No has introducido datos válidos", Toast.LENGTH_LONG).show();
                                                e.printStackTrace();
                                            } catch (SQLException a) {
                                                a.printStackTrace();
                                            }
                                        } else {
                                            Toast.makeText(context, "No has introducido datos válidos", Toast.LENGTH_LONG).show();
                                        }
                                    }
                                })
                                // Strings para @strings
                                .setNegativeButton("Cancelar",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialogBox, int id) {
                                                dialogBox.cancel();
                                            }
                                        });

                        AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
                        alertDialogAndroid.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                        alertDialogAndroid.show();
                    default:
                        break;
                }
                return false;
            }
        });
    }

//        addButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                LayoutInflater layoutInflaterAndroid = LayoutInflater.from(BaseActivity.this);
//                View mView = layoutInflaterAndroid.inflate(R.layout.dialog_article, null);
//                AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(new ContextThemeWrapper(BaseActivity.this, R.style.AppTheme));
//                alertDialogBuilderUserInput.setView(mView);
//                alertDialogBuilderUserInput.setIcon(R.drawable.ic_backspace_white_24dp);
//                fotoArticulo = (ImageView) mView.findViewById(R.id.fotoArticulo);
//                fotoArticulo.setClickable(true);
//                fotoArticulo.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        // Simplificado
//
//                        //Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
//                        //getIntent.setType("image/*");
//                        if (ContextCompat.checkSelfPermission(BaseActivity.this,
//                                Manifest.permission.READ_EXTERNAL_STORAGE)
//                                != PackageManager.PERMISSION_GRANTED) {
//
//                            ActivityCompat.requestPermissions(BaseActivity.this,
//                                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, WRITE_PERMISSION);
//                            System.out.println("Pido permisossss");
//                        } else {
//                            Intent pickIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                            pickIntent.setType("image/*");
//                            pickIntent.putExtra(PHOTO_BYTES, png);
//
//                            startActivityForResult(pickIntent, PICK_PHOTO_ARTICLE);
//                        }
//                    }
//                });
//
//                final EditText nombreNuevoArticulo = (EditText) mView.findViewById(R.id.nombre);
//                final EditText precioNuevoArticulo = (EditText) mView.findViewById(R.id.precio);
//                precioNuevoArticulo.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
//                final ImageView fotoNuevoArticulo = (ImageView) mView.findViewById(R.id.fotoArticulo);
//
//                alertDialogBuilderUserInput
//                        .setCancelable(false)
//                        .setPositiveButton("Añadir", new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialogBox, int id) {
//                                // Garrapiña
//                                if (!nombreNuevoArticulo.getText().toString().isEmpty() && !precioNuevoArticulo.getText().toString().isEmpty()) {
//                                    nombreArticulo = nombreNuevoArticulo.getText().toString();
//                                    try {
//                                        Resources res = getResources();
//                                        precioArticulo = Double.parseDouble(precioNuevoArticulo.getText().toString().replace(",", "."));
//                                        // Guardamos artículo en BD
//                                        // Strings para @strings
//                                        if (db.crearArticulo(new Articulo(nombreArticulo, precioArticulo, tabHost.getCurrentTabTag(), fotoPath))) {
//                                            ImageButton dummy = new ImageButton(BaseActivity.this);
//                                            listaPrecios.put(nombreArticulo, precioArticulo);
//                                            // Garrapiña maxima
//
//                                            int idTabSelected = res.getIdentifier("tabla" + tabHost.getCurrentTabTag(), "id", context.getPackageName());
//                                            TableLayout tabAux = (TableLayout) findViewById(idTabSelected);
//
//                                            addArticuloUI(dummy, tabAux, nombreArticulo, fotoArticulo.getDrawable());
//                                        } else {
//                                            Toast.makeText(context, "No se ha podido crear el artículo en BD", Toast.LENGTH_LONG).show();
//                                        }
//                                    } catch (NumberFormatException e) {
//                                        Toast.makeText(context, "No has introducido datos válidos", Toast.LENGTH_LONG).show();
//                                        e.printStackTrace();
//                                    } catch (SQLException a) {
//                                        a.printStackTrace();
//                                    }
//                                } else {
//                                    Toast.makeText(context, "No has introducido datos válidos", Toast.LENGTH_LONG).show();
//                                }
//                            }
//                        })
//                        // Strings para @strings
//                        .setNegativeButton("Cancelar",
//                                new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialogBox, int id) {
//                                        dialogBox.cancel();
//                                    }
//                                });
//
//                AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
//                alertDialogAndroid.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
//                alertDialogAndroid.show();
//            }
//        });
//    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case READ_PERMISSION: {
                // strings para @strings
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Intent pickIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    pickIntent.setType("image/*");
                    pickIntent.putExtra(PHOTO_BYTES, png);
                    startActivityForResult(pickIntent, PICK_PHOTO_ARTICLE);

                } else {
                    Toast.makeText(this, "No has dado permisos para buscar en el dispositivo", Toast.LENGTH_LONG);
                    fotoArticulo.setEnabled(false);
                }
                return;
            }
        }
    }

    public boolean selectedAmbos() {
        return pressedImage == true && pressedNum == true && textCalc != null && !textCalc.getText().toString().equals("0");
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_PHOTO_ARTICLE) {
            // Elegida foto para el artículo
            Context context = getApplicationContext();
            if (resultCode == RESULT_OK) {
                fileUri = data.getData();
                ContentResolver cR = context.getContentResolver();
                MimeTypeMap mime = MimeTypeMap.getSingleton();
                String type = mime.getExtensionFromMimeType(cR.getType(fileUri));

                //Strings para @strings
                if (type.toLowerCase() == "png") {

                    String[] filePathColumn = {MediaStore.Images.Media.DATA};

                    Cursor cursor = getContentResolver().query(fileUri,
                            filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String picturePath = cursor.getString(columnIndex);
                    cursor.close();
                    Bitmap bmp = BitmapFactory.decodeFile(picturePath);
                    // Tenemos el BMP escalado, aquí habrá que guardarlo
                    Bitmap bmpS = Bitmap.createScaledBitmap(bmp, fotoArticulo.getWidth(), fotoArticulo.getHeight(), false);
                    fotoPath = fileUri.toString();
                    foto = new BitmapDrawable(getResources(), bmpS);
                    System.out.println(fotoPath + " ======= " + fileUri);
                    fotoArticulo.setImageBitmap(bmpS);

                    // Guardamos la imagen, para luego usarla en este botón
                    saveImgToBD(bmpS);
                } else {
                    Toast.makeText(context, "No has seleccionado una imagen PNG", Toast.LENGTH_LONG).show();
                }
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(context, "No has elegido ninguna imagen, no podrás crear el artículo !", Toast.LENGTH_LONG).show();
            }
        }
    }

    public boolean saveImgToBD(Bitmap bmp) {
        //ContextWrapper cw = new ContextWrapper(getApplicationContext());
        //File directory = cw.getDir("images", Context.MODE_PRIVATE);
        File directory = new File(Environment.getExternalStorageDirectory().toString());
        if (!directory.exists()) {
            directory.mkdir();
        }
        String filename = randomStringName();
        File mypath = new File(directory, filename);

        FileOutputStream fos = null;
        try {
            if (ContextCompat.checkSelfPermission(BaseActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(BaseActivity.this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, WRITE_PERMISSION);
                System.out.println("Pido permisos para escribir");
            } else {
                fos = new FileOutputStream(mypath);
                bmp.compress(Bitmap.CompressFormat.PNG, 100, fos);
                fotoPath = mypath.getAbsolutePath();
                System.out.println("He guardado la foto en " + mypath.getAbsolutePath());
                fos.close();
                return true;
            }
        } catch (Exception e) {
            System.out.println("No se ha guardado la imagen");
            Log.e("SAVE_IMAGE", e.getMessage(), e);
        }

        return false;
    }

    public String randomStringName() {

        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 7;
        StringBuilder buffer = new StringBuilder(targetStringLength);
        for (int i = 0; i < targetStringLength; i++) {
            int randomLimitedInt = leftLimit + (int)
                    (new Random().nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        String generatedString = buffer.toString();

        return generatedString;
    }

    public boolean addArticuloUI(ImageButton imageBttn, TableLayout tabla, String tag, Drawable src) {

        imageBttn.setId(View.generateViewId());
        imageBttn.setTag(tag);
        imageBttn.setImageDrawable(src);
        imageBttn.setScaleType(ImageView.ScaleType.FIT_CENTER);
        imageBttn.setLongClickable(true);
        imageBttn.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(new ContextThemeWrapper(BaseActivity.this, R.style.AppTheme));
                final ImageButton backup = (ImageButton) v;
                alertDialogBuilderUserInput
                        .setCancelable(false)
                        .setMessage(R.string.menu_action_borrar)
                        .setPositiveButton("Sí", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                // Sería conveniente comprobar que la BDD es consistente con la UI ...
                                // Strings para @strings
                                final Articulo backupAux = db.getArticulo(backup.getTag().toString());
                                backup.setVisibility(View.GONE);
                                final String aborrar = backup.getTag().toString();
                                db.borrarArticulo(aborrar);

                                CharSequence chars = "Se ha borrado " + aborrar;
                                final FabSpeedDial aux = (FabSpeedDial) findViewById(R.id.addImgButton);
                                Snackbar.make(aux, chars, Snackbar.LENGTH_LONG)
                                        .setAction("DESHACER", new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                backup.setVisibility(View.VISIBLE);
                                                db.crearArticulo(backupAux);
                                                Snackbar.make(aux, "Recuperado " + aborrar, Snackbar.LENGTH_LONG).show();
                                            }
                                        }).show();
                            }
                        }).setNegativeButton("No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                dialogBox.cancel();
                            }
                        });
                AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
                alertDialogAndroid.show();
                return true;
            }
        });

        imageBttn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pressedImage = true;
                String itemLista;
                int itemIndex;
                boolean found;
                if (selectedAmbos()) {
                    // Garrapiña, sustituir por algo más elegante
                    // además está bien pero abusando, si pillo el tag de v no hace falta hacer tratamiento -
                    // la mejor opción sería actualizar pedido y listaArticulos a partir de él
                    screen = textCalc.getText().toString();
                    String tag = v.getTag().toString();
                    if (!listaArticulos.isEmpty()) {
                        found = pedido.containsKey(tag);
                        if (!found) {
                            pedido.put(tag, Integer.valueOf(screen));
                            listaArticulos.add(tag + "\tx" + pedido.get(tag));
                        } else {
                            int aux = Integer.valueOf(screen);
                            listaArticulos.remove(tag + "\tx" + pedido.get(tag));
                            aux += pedido.get(tag);
                            pedido.put(tag, aux);
                            listaArticulos.add(tag + "\tx" + aux);
                        }
                    } else {
                        listaArticulos.add(tag + "\tx" + Integer.valueOf(screen));
                        pedido.put(tag, Integer.valueOf(screen.trim()));
                    }

                    System.out.println(listaArticulos.toString() + "<-- Artículos");
                    System.out.println(pedido.toString() + "<-- Pedido");
                    ((ArrayAdapter) adapter).notifyDataSetChanged();
                    textCalc.setText(null);
                    // Garrapiña redondeo y sacar cosas de articulos y no de pedido
                    textCalc.setText(Math.round((listaPrecios.get(tag)) * Integer.valueOf(screen) * 100.0) / 100.0 + "");
                    resetScreen = true;
                    pressedNum = false;
                } else {
                    Context context = getApplicationContext();
                    Toast.makeText(context, "¿Cuánt@s quieres?", Toast.LENGTH_SHORT).show();
                }
            }
        });

        imageBttn.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT, 0.33f));

        TableRow fila;
        if (elemInRow % 3 == 0) {
            fila = new TableRow(this);
            actualRow = tabla.getChildCount();
            System.out.println("Nueva fila !");
            fila.setLayoutParams(new TableLayout.LayoutParams(
                    TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT, 0.33f));
            imageBttn.setMinimumHeight(32);
            imageBttn.setMaxHeight(32);
            fila.addView(imageBttn);
            tabla.addView(fila);
            elemInRow = fila.getChildCount();
            return true;
        } else {
            fila = (TableRow) tabla.getChildAt(actualRow);
            if (fila == null) {
                fila = new TableRow(this);
                System.out.println("Nueva fila !");
            } else {
                System.out.println("Cojo la fila que hay");
            }
            fila.addView(imageBttn);
            actualRow = tabla.getChildCount();
            elemInRow = fila.getChildCount();
            tabla.removeView(fila);
            tabla.addView(fila);
            return true;
        }
///////////////////////////////////////// no hay return false
    }

    @Override
    public void onBackPressed() {
        if (dobleAtras) {
            super.onBackPressed();
            return;
        }// strings para @strings
        // Aquí habría que salvar el último pedido o el actual en curso
        dobleAtras = true;
        Toast.makeText(this, "Pulsa atrás de nuevo para salir", Toast.LENGTH_LONG).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                dobleAtras = false;
            }
        }, 2000);
    }


    public double getTotal() {
        double aux = 0.0;
        for (Map.Entry<String, Integer> i : pedido.entrySet()) {
            aux += i.getValue() * listaPrecios.get(i.getKey());
        }
        return aux;
    }


}
