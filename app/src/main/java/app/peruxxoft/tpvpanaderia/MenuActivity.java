package app.peruxxoft.tpvpanaderia;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/*################################################################
  #                                                              #
  #             PRESENTATION INTERFACE MENU                      #
  #                                                              #
  ################################################################
 */

public class MenuActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_gestion);
    }
}
