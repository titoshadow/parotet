package data;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by jorge on 14/04/2017.
 */

public class Pedido {

    private Map<Articulo, Integer> articulos = new LinkedHashMap<Articulo, Integer>();
    private double precioT;
    private Cliente cliente;

    public Pedido(Map<Articulo, Integer> articulos, double precioT, Cliente cliente) {
        this.articulos = articulos;
        this.precioT = precioT;
        this.cliente = cliente;
    }

    public Pedido(Map<Articulo, Integer> articulos) {
        this.articulos = articulos;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Map<Articulo, Integer> getArticulos() {
        return articulos;
    }

    public void setArticulos(Map<Articulo, Integer> articulos) {
        this.articulos = articulos;
    }

    public int getCantidad(Articulo articulo){
        return articulos.get(articulo);
    }

    public double getPrecioT() {
        return precioT;
    }

    public void setPrecioT(double precioT) {
        this.precioT = precioT;
    }
}
