package data;

import java.util.UUID;

/**
 * Created by jorge on 14/04/2017.
 */

public class Articulo {

    private String id;
    private String nombre;
    private double precio;
    private String categoria;
    private String uriFoto;

    public Articulo(String nombre, double precio, String categoria, String uriFoto) {
        this.id = UUID.randomUUID().toString();
        this.nombre = nombre;
        this.precio = precio;
        this.categoria = categoria;
        this.uriFoto = uriFoto;
    }

    public String getId(){
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUriFoto() {
        return uriFoto;
    }

    public void setUriFoto(String uriFoto) {
        this.uriFoto = uriFoto;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }
}
