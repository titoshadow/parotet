package data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;

/**
 * Created by jorge on 16/04/2017.
 */

public class ParotetDB extends SQLiteOpenHelper {

    private static final int VERSION_ACTUAL = 1;
    Contract contrato;

    public ParotetDB(Context contexto) {
        super(contexto, Contract.NOMBRE_BASE_DATOS, null, VERSION_ACTUAL);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                db.setForeignKeyConstraintsEnabled(true);
            } else {
                db.execSQL("PRAGMA foreign_keys=ON");
            }
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Contract.ArticuloBD.CREATE_TARTICULO);
        db.execSQL(Contract.ClienteBD.CREATE_TCLIENTE);
        db.execSQL(Contract.PedidoBD.CREATE_TPEDIDO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(Contract.ArticuloBD.DELETE_TABLE);
        db.execSQL(Contract.ClienteBD.DELETE_TABLE);
        db.execSQL(Contract.PedidoBD.DELETE_TABLE);
        onCreate(db);
    }

    public Articulo getArticulo(String nombre) {

        SQLiteDatabase db = this.getReadableDatabase();
        String categoria = null;
        String path = null;
        Cursor cursor = db.rawQuery("SELECT * FROM " + Contract.ArticuloBD.TABLE_NAME + " WHERE nombre = ?", new String[]{nombre});

        if (!cursor.moveToFirst()) {
            return null;
        } else { // Strings para @strings
            double precioD = cursor.getDouble(cursor.getColumnIndex("precio"));
            categoria = cursor.getString(cursor.getColumnIndex("categoria"));
            path = cursor.getString(cursor.getColumnIndex("path"));
            Articulo aux = new Articulo(nombre, precioD, categoria, path);
            return aux;
        }
    }

    public boolean borrarArticulo(String nombre) {

        ContentValues values = new ContentValues();
        values.put(Contract.ArticuloBD.NOMBRE, nombre);

        SQLiteDatabase dbR = this.getReadableDatabase();
        SQLiteDatabase dbW = this.getWritableDatabase();
        String nombreA = nombre;
        Cursor cursor = dbR.rawQuery("SELECT * FROM " + Contract.ArticuloBD.TABLE_NAME + " WHERE nombre = ?", new String[]{nombre});
        if (nombreA.isEmpty()) {
            return false;
        } else {
            dbW.execSQL("DELETE FROM " + Contract.ArticuloBD.TABLE_NAME + " WHERE nombre = ?", new String[]{nombre});
            dbW.close();
            return true;
        }

    }

    public boolean crearPedido(Pedido pedido) {

        ContentValues values = new ContentValues();

        values.put(Contract.PedidoBD.IDCLIENTE, pedido.getCliente().getNombre());
        values.put(Contract.PedidoBD.CONTENIDO, pedido.getArticulos().entrySet().toString());
        values.put(Contract.PedidoBD.PRECIOT, pedido.getPrecioT());

        SQLiteDatabase db = this.getWritableDatabase();

        boolean createSuccessful = db.insert(Contract.ArticuloBD.TABLE_NAME, null, values) > 0;
        db.close();

        return createSuccessful;
    }

    public boolean crearArticulo(Articulo articulo) {

        ContentValues values = new ContentValues();

        values.put(Contract.ArticuloBD.NOMBRE, articulo.getNombre());
        values.put(Contract.ArticuloBD.CATEGORIA, articulo.getCategoria());
        values.put(Contract.ArticuloBD.PRECIO, articulo.getPrecio());
        values.put(Contract.ArticuloBD.FOTOPATH, articulo.getUriFoto());

        SQLiteDatabase db = this.getWritableDatabase();

        boolean createSuccessful = db.insert(Contract.ArticuloBD.TABLE_NAME, null, values) > 0;
        db.close();

        return createSuccessful;
    }

    public boolean crearCliente(Cliente cliente) {

        ContentValues values = new ContentValues();

        values.put(Contract.ClienteBD.NOMBRE, cliente.getNombre());
        values.put(Contract.ClienteBD.APELLIDOS, cliente.getApellidos());
        values.put(Contract.ClienteBD.TELEFONO, cliente.getTelefono());

        SQLiteDatabase db = this.getWritableDatabase();

        boolean createSuccesful = db.insert(Contract.ClienteBD.TABLE_NAME, null, values) > 0;
        db.close();

        return createSuccesful;
    }


    public Cursor leerArticulos() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM Articulo", new String[]{});
        cursor.moveToFirst();
        db.close();
        return cursor;
    }
}
