package data;

import android.provider.BaseColumns;

/**
 * Created by jorge on 16/04/2017.
 */

public class Contract {

    public static final String NOMBRE_BASE_DATOS = "parotet.db";
    public static final int DATABASE_VERSION = 1;
    private static final String TEXT_TYPE = " TEXT";
    private static final String REAL_TYPE = " REAL";
    private static final String NULLCONSTRAINT = " NOT NULL";
    private static final String FKCONSTRAINT = " REFERENCES ";
    private static final String COMMA_SEP = ", ";

    private Contract() {
    }

    public static abstract class ArticuloBD implements BaseColumns {
        public static final String TABLE_NAME = "Articulo";
        public static final String NOMBRE = "nombre";
        public static final String CATEGORIA = "categoria";
        public static final String PRECIO = "precio";
        public static final String FOTOPATH = "path";


        public static final String CREATE_TARTICULO = "CREATE TABLE Articulo (" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT" + COMMA_SEP +
                NOMBRE + TEXT_TYPE + NULLCONSTRAINT + COMMA_SEP +
                CATEGORIA + TEXT_TYPE + NULLCONSTRAINT + COMMA_SEP +
                PRECIO + REAL_TYPE + NULLCONSTRAINT + COMMA_SEP +
                FOTOPATH + TEXT_TYPE + NULLCONSTRAINT + " )";
        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    public static abstract class PedidoBD implements BaseColumns {
        public static final String TABLE_NAME = "Pedido";
        public static final String IDCLIENTE = "idC";
        public static final String CONTENIDO = "contenido";
        public static final String PRECIOT = "preciot";

        public static final String CREATE_TPEDIDO = "CREATE TABLE " +
                TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT" + COMMA_SEP +
                IDCLIENTE + TEXT_TYPE + NULLCONSTRAINT + FKCONSTRAINT + ClienteBD._ID + COMMA_SEP +
                CONTENIDO + TEXT_TYPE + NULLCONSTRAINT + COMMA_SEP +
                PRECIOT + REAL_TYPE + NULLCONSTRAINT + " )";
        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    public static abstract class ClienteBD implements BaseColumns {
        public static final String TABLE_NAME = "Cliente";
        public static final String NOMBRE = "nombre";
        public static final String APELLIDOS = "apellidos";
        public static final String TELEFONO = "telefono";

        public static final String CREATE_TCLIENTE = "CREATE TABLE " +
                TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT" + COMMA_SEP +
                NOMBRE + TEXT_TYPE + NULLCONSTRAINT + COMMA_SEP +
                APELLIDOS + TEXT_TYPE + COMMA_SEP +
                TELEFONO + TEXT_TYPE + " )";
        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }
}
